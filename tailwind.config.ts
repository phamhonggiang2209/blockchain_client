import { motion } from "framer-motion";

// tailwind.config.js
const {nextui} = require("@nextui-org/react");
const {commonColors} = require("@nextui-org/theme/colors");
const defaultTheme = require("tailwindcss/defaultTheme");

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    // ...
    "./node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}",
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./content/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      input: {
        backgroundColor: "#FFF"
      },
      boxShadow: {
        highlighted: `${commonColors.purple[500]} 1px 0 0, ${commonColors.purple[500]} -1px 0 0`,
      },
      fontFamily: {
        sans: ["Open Sans", ...defaultTheme.fontFamily.sans, "sans-serif"],
        serif: defaultTheme.fontFamily.serif,
        mono: defaultTheme.fontFamily.mono,
      },
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic": "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
      keyframes: {
        heartbeat: {
          "0%": {transform: "scale(1)"},
          "50%": {transform: "scale(1.2)"},
          "100%": {transform: "scale(1)"},
        },
        rotate: {
          "0%": {
            transform: "rotate(0deg)",
          },
          "50%": {
            transform: "rotate(360deg)",
          },
          "100%": {
            transform: "rotate(0deg)",
          },
        },
        levitate: {
          "0%": {
            transform: "translateX(0)",
          },
          "30%": {
            transform: "translateX(-15px)",
          },
          "50%": {
            transform: "translateX(9px)",
          },
          "70%": {
            transform: "translateX(-20px)",
          },
          "100%": {
            transform: "translateX(0)",
          },
        },
        expand: {
          "0%": {transform: "scale(1)"},
          "50%": {transform: "scale(1.2)"},
          "100%": {transform: "scale(1)"},
        },
        "expand-opacity": {
          "0%": {
            opacity: 0,
            transform: "scale(1)",
          },
          "50%": {
            opacity: 1,
            transform: "scale(1.3)",
          },
          "100%": {
            opacity: 0,
            transform: "scale(1.295)",
          },
        },
      },
      animation: {
        heartbeat: "heartbeat 1s ease-in-out infinite",
        rotate: "rotate 6s ease infinite",
        levitate: "levitate 10s ease infinite",
        expand: "expand 6s ease-out infinite both",
        "exand-opacity": "expand-opacity 6s linear infinite both",
      },
    },
  },
  plugins: [
    nextui({
      addCommonColors: true
    })
  ],
  darkMode: "class",
};