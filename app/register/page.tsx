'use client'
import React, {useState, useEffect} from "react";
import { Button, Input } from "@nextui-org/react";
import { Form, Formik } from "formik";
function Register() {
  // Render your article details using the id and slug
  const [isVisible, setIsVisible] = useState(false);

  const submitForm = (e: any) => {
    console.log(e);
  }
  return (
    <>
       <div className="container mx-auto max-w-3xl px-6 pb-[3rem] mb-[2rem] bg-slate-200 rounded">
        <div className="mt-[8rem]">
          <div className="">
            <h1 className="text-center block text-success block font-semibold text-[2rem] lg:leading-[5rem]">
              Đăng ký thông tin tài khoản
            </h1>
          </div>
          <div className="form-content mt-[1rem]">
            <Formik initialValues={{
              email: "",
              fullname: "",
              phone_number: "",
              password: "",
              password_confirm: ""
            }} onSubmit={submitForm}>
              {({
                values,
                handleChange,
                handleSubmit,
                isSubmitting,
              }) => (
                <Form onSubmit={handleSubmit}>
                  <Input
                    classNames={{
                      inputWrapper: "bg-white"
                    }}
                    isRequired
                    name="email"
                    labelPlacement="outside"
                    color="default"
                    label="Email"
                    placeholder="Nhập địa chỉ email"
                    onChange={handleChange}
                    value={values.email}
                  />
                  <Input
                    isRequired
                    classNames={{
                      inputWrapper: "bg-white"
                    }}
                    name="fullname"
                    labelPlacement="outside"
                    className="mt-3"
                    label="Họ và tên"
                    placeholder="Nhập địa họ và tên"
                    onChange={handleChange}
                    value={values.fullname}
                  />
                  <Input
                    labelPlacement="outside"
                    classNames={{
                      inputWrapper: "bg-white"
                    }}
                    className="mt-3"
                    label="Số điện thoại"
                    placeholder="Nhập số điện thoại"
                    name="phone_number"
                    onChange={handleChange}
                    value={values.phone_number}
                  />
                  <Input
                    isRequired
                    classNames={{
                      inputWrapper: "bg-white"
                    }}
                    labelPlacement="outside"
                    className="mt-3"
                    label="Mật khẩu"
                    placeholder="Nhập mật khẩu"
                    type="password"
                    name="password"
                    onChange={handleChange}
                    value={values.password}
                  />
                  <Input
                    isRequired
                    classNames={{
                      inputWrapper: "bg-white"
                    }}
                    labelPlacement="outside"
                    className="mt-3"
                    label="Kiểm tra Mật khẩu"
                    placeholder="Nhập lại mật khẩu"
                    type="password"
                    name="password_confirm"
                    onChange={handleChange}
                    value={values.password_confirm}
                  />
                  <div className="flex justify-center mt-4">
                    <Button color="success" className="mt-3" type="submit" disabled={isSubmitting}>
                      Đăng ký
                    </Button>
                  </div>
                </Form>
                )}
            </Formik>
          </div>
        </div>
      </div>
    </>
  );
}

export default Register;
