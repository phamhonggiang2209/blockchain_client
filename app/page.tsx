import Articles from '@/content/articles';
import Banner from '@/content/banner';
import SupportForm from '@/components/forms/support';
import PricingsPlanWrapper from '@/content/pricingsPlan';
import Question from '@/content/question';
import ServiceWrapper from '@/content/serviceWrapper';
import Link from 'next/link';
import { FaArrowRight } from 'react-icons/fa';
import { IoChatbubblesOutline } from 'react-icons/io5';
// import NavBarLayout from './layouts/navBarLayout';

const Home = () => {
  return (
    <>
      <div className="home-container" key="home-container">
        <Banner />
        <ServiceWrapper />
        <PricingsPlanWrapper />
        <Articles />
        <Question />
        <div className="inbox-swapper bg-cover bg-no-repeat bg-center"
          style={{
            backgroundImage: "url('/inbox-bg.png')"
          }}
        >
          <div className="container mx-auto max-w-3xl text-center px-6">
            <div className="py-[8rem]">
              <h1 className="block font-semibold text-[2rem] lg:leading-[3rem]">
                Tối đa hóa hiệu quả marketing cùng 
                <span className="text-success"> 400,000+ </span> 
                khách hàng của chúng tôi
              </h1>
              <div className="py-[2rem]">
                <div
                  className="z-0 group relative inline-flex items-center justify-center box-border appearance-none select-none whitespace-nowrap font-normal subpixel-antialiased overflow-hidden tap-highlight-transparent outline-none data-[focus-visible=true]:z-10 data-[focus-visible=true]:outline-2 data-[focus-visible=true]:outline-focus data-[focus-visible=true]:outline-offset-2 px-unit-6 min-w-unit-24 h-unit-12 text-medium gap-unit-3 rounded-full [&>svg]:max-w-[theme(spacing.unit-8)] data-[pressed=true]:scale-[0.97] transition-transform-colors motion-reduce:transition-none bg-success text-primary-foreground w-full md:w-auto"
                >
                  Bắt đầu trải nghiệm ngay
                  <FaArrowRight />
                </div>
              </div>
              <div>
                <Link className="text-xs text-warning" href="#">
                  <IoChatbubblesOutline />
                  Livechat để được tư vấn ngay
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
      <SupportForm key="support-form-container" />
    </>
  )
}

export default Home;