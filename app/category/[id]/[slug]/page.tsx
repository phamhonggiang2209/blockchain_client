'use client'
/* eslint-disable @next/next/no-img-element */
import { ReactNode } from "react";

import NavBarLayout from '@/app/layouts/navBarLayout';
import { Metadata, Route } from 'next';
import { ArticleData, ListArticlesType } from "@/data/article.mock";
import ArticleItems from "@/components/articleItem";
import { Button, Link } from "@nextui-org/react";

interface CategoryProps {
  params: {
    id: string;
    slug: string;
  };
}

const SliderItems = (): ReactNode[] => {
  return ArticleData.map((item:ListArticlesType, index: number) => {
    return <ArticleItems
      key={item.title + "_" + index}
      index={index}
      title={item.title}
      link={item.link}
      image={item.image}
      tag={item.tag}
      desc={item.desc}
      author={item.author}
    />
  });
}

export default function Categories({params}: CategoryProps) {
  // Render your article details using the id and slug
  const {id, slug} = params;

  return (
    <div className="relative bg-white pt-[6rem]">
      <div className="category-banner relative">
        <img src="/category-banner.png" alt="" className="w-[100%] rounded-none" />
        <div className="absolute w-full h-full top-0 left-0 flex items-center">
          <div className="container mx-auto max-w-7xl text-white mx-auto">
            <div className="ml-3">
              <h1 className="inline font-semibold text-[3rem] lg:leading-[4rem]">Danh sách bài viết</h1>
              {/* <span className="block">Mô tả</span> */}
            </div>
          </div>
        </div>
      </div>

      <div className="category-content container container mx-auto max-w-7xl mt-10">
        <div className="category-content-action mb-10">
          <div className="flex items-center gap-3 px-3">
            <Link href="/admin/new/store">
              <Button color="success" href="/register" variant="flat">
                Tạo mới
              </Button>
            </Link>
            
            <Button color="default" href="/register" variant="flat" disabled>
              Xóa
            </Button>
          </div>
        </div>
        <div className="category-content-list grid grid-cols-3 gap-2">
          { SliderItems() }
        </div>
      </div>
    </div>
  );
};
