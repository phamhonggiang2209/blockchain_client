export interface ListArticlesType {
  tag: string;
  link: string;
  title: string;
  desc: string;
  author: any;
  create_at: string;
  image: string;
};

export const ArticleData:ListArticlesType[] = [
  {
    tag: "Forex trading",
    link: "#",
    title: "Swing Trading Definition",
    desc: "Our platform is not only about trading—it's also a hub for knowledge and learning. We provide resources.",
    author: {
      name: "Vasha Gueye",
      avt: "/author.png"
    },
    create_at: "Forex trading20/6/2023",
    image: "/home/article/1.png"
  },
  {
    tag: "Trading market",
    link: "#",
    title: "hedge funds work?",
    desc: "To cater to your individual trading preferences, we offer a variety of order types, including market.",
    author: {
      name: "Abhivibha Kanmani",
      avt: "/author.png"
    },
    create_at: "Trading market30/5/2023",
    image: "/home/article/2.png"
  },
  {
    tag: "Investment",
    link: "#",
    title: "Options Trading business?",
    desc: "Security is our top priority, and we employ robust measures to ensure the safety of your funds.",
    author: {
      name: "Hulya Aydin",
      avt: "/author.png"
    },
    create_at: "Investment12/07/2023",
    image: "/home/article/3.png"
  },
  {
    tag: "Forex trading",
    link: "#",
    title: "Swing Trading Definition",
    desc: "Our platform is not only about trading—it's also a hub for knowledge and learning. We provide resources.",
    author: {
      name: "Vasha Gueye",
      avt: "/author.png"
    },
    create_at: "Forex trading20/6/2023",
    image: "/home/article/4.png"
  },
  {
    tag: "Trading market",
    link: "#",
    title: "hedge funds work?",
    desc: "To cater to your individual trading preferences, we offer a variety of order types, including market.",
    author: {
      name: "Abhivibha Kanmani",
      avt: "/author.png"
    },
    create_at: "Trading market30/5/2023",
    image: "/home/article/5.png"
  },
  {
    tag: "Investment",
    link: "#",
    title: "Options Trading business?",
    desc: "Security is our top priority, and we employ robust measures to ensure the safety of your funds.",
    author: {
      name: "Hulya Aydin",
      avt: "/author.png"
    },
    create_at: "Investment12/07/2023",
    image: "/home/article/6.png"
  },
  {
    tag: "Forex trading",
    link: "#",
    title: "Swing Trading Definition",
    desc: "Our platform is not only about trading—it's also a hub for knowledge and learning. We provide resources.",
    author: {
      name: "Vasha Gueye",
      avt: "/author.png"
    },
    create_at: "Forex trading20/6/2023",
    image: "/home/article/7.png"
  },
  {
    tag: "Trading market",
    link: "#",
    title: "hedge funds work?",
    desc: "To cater to your individual trading preferences, we offer a variety of order types, including market.",
    author: {
      name: "Abhivibha Kanmani",
      avt: "/author.png"
    },
    create_at: "Trading market30/5/2023",
    image: "/home/article/8.png"
  },
  {
    tag: "Investment",
    link: "#",
    title: "Options Trading business?",
    desc: "Security is our top priority, and we employ robust measures to ensure the safety of your funds.",
    author: {
      name: "Hulya Aydin",
      avt: "/author.png"
    },
    create_at: "Investment12/07/2023",
    image: "/home/article/9.png"
  },
];