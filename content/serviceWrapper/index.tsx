'use client'

import { Image, Link } from "@nextui-org/react";

export default function ServiceWrapper () {
  const data = [
    {
      image: "/service/1.png",
      link: "#",
      title: "Strategy Consulting",
      desc: "A social assistant that's flexible can accommodate your schedule and needs, making life easier."
    },
    {
      image: "/service/2.png",
      link: "#",
      title: " Financial Advisory",
      desc: "Modules transform smart trading by automating processes and improving decision-making."
    },
    {
      image: "/service/3.png",
      link: "#",
      title: "Management",
      desc: "There, it's me, your friendly neighborhood reporter's news analyst processes and improving"
    },
    {
      image: "/service/4.png",
      link: "#",
      title: "Supply Optimization",
      desc: "Hey, have you checked out that new cryptocurrency platform? It's pretty cool and easy to use!"
    },
    {
      image: "/service/5.png",
      link: "#",
      title: "Supply Optimization",
      desc: "Hey, have you checked out that new cryptocurrency platform? It's pretty cool and easy to use!"
    },
    {
      image: "/service/6.png",
      link: "#",
      title: "Supply Optimization",
      desc: "Hey, have you checked out that new cryptocurrency platform? It's pretty cool and easy to use!"
    },
  ];

  return (
    <div className="service-wrapper">
      <div className={`h-100 container mx-auto max-w-7xl px-6 py-[8rem]`}>
        <div className="service-content-title text-center mx-auto" style={{
          maxInlineSize: "50ch"
        }}>
          <h1 className="banner__content__heading block font-semibold text-[3rem] lg:leading-[4rem] ">
            <span className="inline font-semibold from-[#0a4fd58f] to-[#0A4FD5] text-[3rem] bg-clip-text text-transparent bg-gradient-to-b">Services </span> 
            We Offer
          </h1>
          <div className="py-7 w-full md:w-1/2 my-2 text-lg max-w-full !w-full text-slate-500 font-thin">
            We offer the best services around - from installations to repairs, maintenance, and more!
          </div>
        </div>
        <div className="service-content-data pt-5">
          <div className="grid grid-cols-3 gap-x-5 gap-y-5">
            {data.map((item:any, index: number) => {
              return <Link 
                key={item.title + "_" + index}
                href={item.link} 
                className="text-current rounded-lg border hover:-translate-y-2 ease-in-out transition-transform border-transparent hover:border-success bg-gradient-to-b from-[#edf2f859] to-[#EDF2F8] text-center py-[4em] px-7 block" 
                
              >
                <div className="p-7 rounded-full bg-white place-items-center mb-3 inline-block">
                  <Image src={item.image} alt={item.title} className="rounded-none"/>
                </div>
                <div className="py-[1.2em] font-bold text-[1.5rem]">{item.title}</div>
                <p className="text-slate-400">{item.desc}</p>
              </Link>
            })}
          </div>
        </div>
      </div>
    </div>
  )
}