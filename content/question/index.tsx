'use client'

import { Accordion, AccordionItem, Image } from "@nextui-org/react";
import { useEffect, useState } from "react";
import { HTMLMotionProps } from "framer-motion";
import { FaFacebookF, FaLinkedinIn, FaInstagram, FaYoutube, FaTwitter, FaPlus, FaMinus } from 'react-icons/fa';

export type MotionProps = HTMLMotionProps<"div">;

export default function Question () {
  const [navHeight, setNavHeight] = useState<number>(0);
  const data = [
    {
      title: "What does this tool do?",
      desc: "Online trading’s primary advantages are that it allows you to manage your trades at your convenience"
    },
    {
      title: "What are the disadvantages of online trading?",
      desc: "You don’t need to worry, the interface is user-friendly. Anyone can use it smoothly. Our user manual will help you to solve your problem"
    },
    {
      title: "Is online trading safe?",
      desc: "Online trading’s primary advantages are that it allows you to manage your trades at your convenience."
    },
    {
      title: "What is online trading, and how does it work?",
      desc: "Online trading’s primary advantages are that it allows you to manage your trades at your convenience."
    },
    {
      title: "Which app is best for online trading?",
      desc: "Online trading’s primary advantages are that it allows you to manage your trades at your convenience."
    },
    {
      title: "How to create a trading account?",
      desc: "Online trading’s primary advantages are that it allows you to manage your trades at your convenience."
    }
  ];

  const followData = [
    { icon: <FaFacebookF className="align-middle"/>, link: "#" },
    { icon: <FaLinkedinIn className="align-middle"/>, link: "#" },
    { icon: <FaInstagram className="align-middle"/>, link: "#" },
    { icon: <FaYoutube className="align-middle"/>, link: "#" },
    { icon: <FaTwitter className="align-middle"/>, link: "#" }
  ];
  
  useEffect(() => {
    const navElm = document.getElementById('endUserNav');
    const height:number|undefined = navElm?.offsetHeight;
    setNavHeight(height || 0);
  }, []);

  return (
    <>
      <div className="question-wrapper pb-10">
        <div className="container mx-auto max-w-7xl px-6">
          <div className="motion-reduce:transition-none inline-block animate-levitate absolute mt-10">
            <Image src="/question/2.png" alt="coin image"/>
          </div>
          <div className="pricings-plan-content-title text-center mx-auto" style={{
            maxInlineSize: "60ch"
          }}>
            <h1 className="banner__content__heading block font-semibold text-[3rem] lg:leading-[4rem] ">
              <span className="inline font-semibold from-[#0a4fd58f] to-[#0A4FD5] text-[3rem] bg-clip-text text-transparent bg-gradient-to-b">Frequently </span> 
              Asked Questions
            </h1>
            <div className="py-7 w-full my-2 text-lg text-slate-500 font-thin">
              Hey there! Got questions? we did got answers. Check out our FAQ page for all the deets. Still not satisfied? Hit us up.
            </div>
          </div>
          <div className="grid grid-cols-2 gap-2 xl:py-10 items-center">
            <div className="question__content md:h-[400px]">
                <Accordion defaultSelectedKeys={["0"]}>
                  {
                    data.map((item, index) => {
                      return <AccordionItem
                        className="font-bold"
                        key={index + ""}
                        aria-label={item.title}
                        title={item.title}
                        indicator={({ isOpen }: any) => (isOpen ? <FaMinus /> : <FaPlus />)}
                      >
                        <span className="text-md text-slate-400 font-normal pb-5 block">{item.desc}</span>
                      </AccordionItem>
                    })
                  }
                </Accordion>
            </div>
            <div className="relative">
              <div className="faq__thumb faq__thumb--style1 flex justify-center">
                <Image src="/question/1.png" alt="" className="inline-block" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}