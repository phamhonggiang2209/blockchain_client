'use client'

import {  Link } from "@nextui-org/react";
import { ReactNode } from "react";
import { HTMLMotionProps } from "framer-motion";
import { FaArrowRight, FaChevronLeft, FaChevronRight } from "react-icons/fa";
import AliceCarousel from "react-alice-carousel";
import { ArticleData, ListArticlesType } from "@/data/article.mock";
import ArticleItems from "@/components/articleItem";

export type MotionProps = HTMLMotionProps<"div">;

const SliderItems = (): ReactNode[] => {
  return ArticleData.map((item:ListArticlesType, index: number) => {
    return <ArticleItems
      key={item.title + "_" + index}
      index={index}
      title={item.title}
      link={item.link}
      image={item.image}
      tag={item.tag}
      desc={item.desc}
      author={item.author}
    />
  });
}

export default function Articles (): ReactNode {
  const responsive = {
    0: { items: 1 },
    568: { items: 2 },
    1024: { items: 3 },
  };

  return (
    <>
      <div className="articals-wrapper relative overflow-hidden bg-white">
        <div className={`h-100 container mx-auto max-w-7xl px-6 pb-[8rem] pt-[3rem] relative z-10`}>
          <div className="articles-content-title" style={{
            maxInlineSize: "60ch"
          }}>
            <h1 className="banner__content__heading block font-semibold text-[3rem] lg:leading-[4rem] ">
              <span className="inline font-semibold from-[#0a4fd58f] to-[#0A4FD5] text-[3rem] bg-clip-text text-transparent bg-gradient-to-b">Articles </span> 
              For Pro Traders
            </h1>
            <div className="py-7 w-full md:w-1/2 my-2 text-lg max-w-full !w-full text-slate-500 font-thin">
              We offer the best pricings around - from installations to repairs, maintenance, and more!
            </div>
          </div>

          <div className="pricings-plan-content-data mt-10 my-5 relative">
            <AliceCarousel
              ssrSilentMode={false}
              // disableButtonsControls
              disableDotsControls
              items={SliderItems()}
              responsive={responsive}
              controlsStrategy="responsive"
              autoPlay={true}
              autoPlayInterval={5000}
              infinite={true}
              keyboardNavigation={true}
              renderPrevButton={() => {
                return <div className="absolute top-1/2 translate-y-[-50%] left-[-30px] w-[40px] h-[40px] rounded-full border hover:bg-success hover:text-white grid place-items-center bg-success text-white">
                  <FaChevronLeft />
                </div>
              }}
              renderNextButton={() => {
                return <div className="absolute top-1/2 translate-y-[-50%] right-[-30px] w-[40px] h-[40px] rounded-full border hover:bg-success hover:text-white grid place-items-center bg-success text-white">
                  <FaChevronRight />
                </div>
              }}
            />
          </div>
          <div className="text-center">
            <Link
              href="#"
              className="z-0 group relative inline-flex items-center justify-center box-border appearance-none select-none whitespace-nowrap font-normal subpixel-antialiased overflow-hidden tap-highlight-transparent outline-none data-[focus-visible=true]:z-10 data-[focus-visible=true]:outline-2 data-[focus-visible=true]:outline-focus data-[focus-visible=true]:outline-offset-2 px-unit-6 min-w-unit-24 h-unit-12 text-medium gap-unit-3 rounded-full [&>svg]:max-w-[theme(spacing.unit-8)] data-[pressed=true]:scale-[0.97] transition-transform-colors motion-reduce:transition-none bg-success text-primary-foreground w-full md:w-auto"
            >
              View More
              <FaArrowRight />
            </Link>
          </div>
        </div>
        <div className="blog__shape">
          <span className="absolute blog__shape-item blog__shape-item--1 w-[1000px] xl:top-[-17%] xl:left-[0%] lg:h-[95%] lg:top-[-17%] lg:left-[-10%] md:top-[-6%] md:left-[-28%] xs:block xs:top-[-14%] xs:left-[-36%]">
            <span className="absolute xs:top-[3%] xs-left[-1%]"></span>
          </span>
        </div>
      </div>
    </>
  )
}