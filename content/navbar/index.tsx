'use client'

import React from "react";
import {Navbar, NavbarContent, NavbarItem, Link, Button, useDisclosure, Tooltip} from "@nextui-org/react";
import {Image} from "@nextui-org/react";
import { MenuItem, menuData } from "./data";
import { HTMLMotionProps } from "framer-motion";
import LoginModal from "../login";
export type MotionProps = HTMLMotionProps<"div">;

export default function App() {
  const {isOpen, onOpen, onOpenChange} = useDisclosure();
  return (
    <>
      <Navbar className="py-4 bg-transparent fixed shadow shadow-success-500/40" maxWidth="xl" id="endUserNav">
        <div className="container mx-auto flex">
          <Link href="/" className="block">
            <Image src="/logo/logo.png" alt="Logo"/>
          </Link>
          <NavbarContent className="md:flex gap-9" justify="end">
            {
              menuData.map((v: MenuItem, index: number) => {
                const child = v.child || [];
                return <NavbarItem key={v.name + "_" + index}>
                <Tooltip
                  // showArrow
                  delay={100}
                  className="my-tooltip py-2"
                  content={child && child.map((item: MenuItem) => {
                    return  <div key={item.name} className="text-left hover:bg-success-200 py-2 w-full px-4 rounded">
                      <Link
                        className="text-left"
                        color="foreground" 
                        aria-current="page" 
                        href={item.link}
                      >
                        {item.name}
                      </Link>
                    </div>
                  })}
                >
                  <Link color="foreground" aria-current="page" href={v.link} className="font-bold fs-xs hover:text-success">
                    {v.name}
                  </Link>
                </Tooltip>
              </NavbarItem>
              })
            }
            <NavbarItem>
              <Link href="/register">
                <Button color="success" href="/register" variant="flat">
                  Sign Up
                </Button>
              </Link>
            </NavbarItem>
            <NavbarItem className="hidden sm:flex">
              <Button color="success" onPress={onOpen}>Sign in</Button>
            </NavbarItem>
          </NavbarContent>
        </div>
      </Navbar>
      <LoginModal 
        isOpen={isOpen}
        onOpenChange={onOpenChange}
      />
    </>
    
  );
}
