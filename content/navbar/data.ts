export interface MenuItem {
  link: string;
  name: string;
  child?: MenuItem[]
}
export const menuData: MenuItem[] = [
  {
    link: '/category/1/tin-tuc',
    name: 'Tin tức',
    child: [
      {
        link: '/category/2/thi-truong',
        name: 'Thị trường',
      },
      {
        link: '/category/3/tin-hieu-hom-nay',
        name: 'Tín hiệu hôm nay',
      },
      {
        link: "/category/4/phan-tich-du-an",
        name: "Phân tích dự án"
      }
    ]
  },
  {
    link: '/category/5/video-day-trading',
    name: 'Video dạy trading',
    child: [
      {
        link: '/category/6/nen-that-va-thuc-hanh',
        name: 'Nến nhật và thực hành',
      },
      {
        link: '/category/7/ma-va-thuc-hanh',
        name: 'MA và thực hành',
      },
      {
        link: "/category/8/rsi-va-thuc-hanh",
        name: "RSI và thực hành"
      },
      {
        link: "/category/9/macd-va-thuc-hanh",
        name: "MACD và thực hành"
      }
    ]
  },
  {
    link: '/category/10/goi-tu-van',
    name: 'Gói tư vấn',
    child: [
      {
        link: '/category/11/chi-tiet-cac-goi-tu-van',
        name: 'Chi tiết các gói tư vấn',
      },
      {
        link: '/category/12/huong-dan-thanh-toan',
        name: 'Hướng dẫn thanh toán',
      }
    ]
  },
  {
    link: '/category/13/tao-tai-khoan-trading',
    name: 'Tạo tài khoản trading',
    child: [
      {
        link: '/category/14/huong-dan-tao-tai-khoan',
        name: 'Hướng dẫn tạo tài khoản',
      }
    ]
  },
  {
    link: '/category/15/cong-dong-trading',
    name: 'Cộng đồng trading',
    child: [
      {
        link: '/news',
        name: 'Telegram',
      },
      {
        link: '/news',
        name: 'Kênh Twitter',
      },
      {
        link: '/news',
        name: 'Facebook',
      },
      {
        link: '/news',
        name: 'Nhóm Zalo',
      }
    ]
  }
]