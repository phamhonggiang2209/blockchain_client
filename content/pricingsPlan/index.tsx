'use client'

import { Link } from "@nextui-org/react";
import { HTMLMotionProps } from "framer-motion";
import { FaCheck } from 'react-icons/fa';
export type MotionProps = HTMLMotionProps<"div">;

export default function PricingsPlanWrapper () {
  interface ListArticlesType {
    price: string;
    link: string;
    title: string;
    service: string[];
  };

  const data:ListArticlesType[] = [
    {
      price: "$99",
      link: "#",
      title: "BASIC",
      service: ["Weekly online meeting", "Unlimited learning access", "24/7 technical support", "Personal training"]
    },
    {
      price: "$149",
      link: "#",
      title: "STANDARD",
      service: ["Weekly online meeting", "Unlimited learning access", "24/7 technical support", "Personal training", "Business analysis"]
    },
    {
      price: "$199",
      link: "#",
      title: "PREMIUM",
      service: ["With all standard features", "Unlimited learning access", "24/7 technical support", "Personal training"]
    },
  ];

  return (
    <>
      <div className="pricings-plan-wrapper relative">
        <div className={`h-100 container mx-auto max-w-7xl px-6 pb-[8rem] relative z-10`}>
          <div className="pricings-plan-content-title text-center mx-auto" style={{
            maxInlineSize: "50ch"
          }}>
            <h1 className="banner__content__heading block font-semibold text-[3rem] lg:leading-[4rem] ">
              Our
              <span className="inline font-semibold from-[#0a4fd58f] to-[#0A4FD5] text-[3rem] bg-clip-text text-transparent bg-gradient-to-b"> Pricings </span> 
              Plan
            </h1>
            <div className="py-7 w-full md:w-1/2 my-2 text-lg max-w-full !w-full text-slate-500 font-thin">
              We offer the best pricings around - from installations to repairs, maintenance, and more!
            </div>
          </div>

          <div className="pricings-plan-content-data pt-10">
            <div className="grid grid-cols-3 gap-x-5 gap-y-5">
              {data.map((item:ListArticlesType, index: number) => {
                const services:string[] = item.service;
                return <Link 
                  key={item.title + "_" + index}
                  href={item.link} 
                  className={`text-current transition-colors ease-in-out bg-white shadow-[0px_32px_80px_0px_#1a40891f] rounded-lg border-2 border-transparent hover:border-success py-[3em] px-7 block ${index === 1 ? 'border-success scale-y-110' : ''}`}
                >
                  <h2 className="pricings-content-title font-bold text-[1.2rem]">{item.title}</h2>
                  <div className="pricings-content-price py-[1.7rem]">
                    <h2>
                      <span className="text-success font-bold text-[2rem]">{item.price}/</span>
                      <span className="text-slate-400">Monthly</span>
                    </h2>
                  </div>
                  <div className="pricings-content-services border-t-1 pt-[2rem]">
                    {services && services.map((ser: string, i: number) => {
                      return <div className="flex pb-4 items-center" key={ser + "_" + index}>
                        <FaCheck className="text-success mr-4"/>
                        <div>{ser}</div>
                      </div>
                    })}
                  </div>
                  <div className={`z-0 mt-[2rem] border rounded-md text-center py-[.8rem] font-[600] border-success hover:bg-success transition-colors ease-in-out ${index === 1 ? ' bg-success-400' : ''}`}>
                    Choose Plan
                  </div>
                </Link>
              })}
            </div>
          </div>
        </div>
        <div className="pricing__shape-item pricing__shape-item--1 xs:block xl:w-[1050px] xs:top-[3%] xs:left-[6%] absolute xl:left-[27%] lg:left-[11%] lg:top-[8%] md:top-[8%] md:left-[12%] ">
          <span className="pricing-bg absolute xl:w-[1000px] md:top-[2%] md-left[2%] h-[96%]"></span>
        </div>
      </div>
    </>
  )
}