'use client'

import { Image, Link } from "@nextui-org/react";
import { HTMLMotionProps } from "framer-motion";
import { FaFacebookF, FaLinkedinIn, FaInstagram, FaYoutube, FaTwitter, FaArrowRight, FaPlay } from 'react-icons/fa';

export type MotionProps = HTMLMotionProps<"div">;

export default function Banner () {

  const followData = [
    { icon: <FaFacebookF className="align-middle"/>, link: "#" },
    { icon: <FaLinkedinIn className="align-middle"/>, link: "#" },
    { icon: <FaInstagram className="align-middle"/>, link: "#" },
    { icon: <FaYoutube className="align-middle"/>, link: "#" },
    { icon: <FaTwitter className="align-middle"/>, link: "#" }
  ];

  return (
    <>
      <div className="banner-wrapper h-screen">
        <div className={`h-100 container mx-auto max-w-7xl px-6 pt-[8rem]`}>
          <div className="grid grid-cols-2 gap-2 xl:pt-10 h-100">
            <div className="banner__content aos-init aos-animate" data-aos="fade-right" data-aos-duration="1000">
              <div className="motion-reduce:transition-none inline-block animate-rotate">
                <Image
                  loading="lazy"
                  src="/home/banner/coin.png" 
                  alt="coin image"
                />
              </div>
              <div className="text-center md:text-left">
                <h1 className="banner__content__heading inline font-semibold text-[3rem] lg:leading-[4rem]">
                  Invest your money <br/> with
                  <span className=" inline font-semibold from-[#0a4fd58f] to-[#0A4FD5] text-[3rem] bg-clip-text text-transparent bg-gradient-to-b"> Higher return</span>
                </h1>
                <h2 className="py-4 w-full md:w-1/2 my-2 text-lg lg:text-xl font-normal text-default-500 block max-w-full !w-full text-center md:text-left">
                  Anyone can invest money to different currency to increase their earnings by the help of Bitrader through online.
                </h2>
                <div className="flex flex-col md:flex-row items-center gap-4 pt-2">
                  <Link
                    href="#"
                    className="z-0 group relative inline-flex items-center justify-center box-border appearance-none select-none whitespace-nowrap font-normal subpixel-antialiased overflow-hidden tap-highlight-transparent outline-none data-[focus-visible=true]:z-10 data-[focus-visible=true]:outline-2 data-[focus-visible=true]:outline-focus data-[focus-visible=true]:outline-offset-2 px-unit-6 min-w-unit-24 h-unit-12 text-medium gap-unit-3 rounded-full [&>svg]:max-w-[theme(spacing.unit-8)] data-[pressed=true]:scale-[0.97] transition-transform-colors motion-reduce:transition-none bg-success text-primary-foreground w-full md:w-auto"
                  >
                    Get Started
                    <FaArrowRight />
                  </Link>
                  <Link
                    href="#"
                    className="tap-highlight-transparent no-underline hover:opacity-80 active:opacity-disabled transition-opacity z-0 group relative inline-flex items-center justify-center box-border appearance-none select-none whitespace-nowrap font-normal subpixel-antialiased overflow-hidden tap-highlight-transparent outline-none data-[focus-visible=true]:z-10 data-[focus-visible=true]:outline-2 data-[focus-visible=true]:outline-focus data-[focus-visible=true]:outline-offset-2 border-medium px-unit-6 min-w-unit-24 h-unit-12 text-medium gap-unit-3 rounded-full [&>svg]:max-w-[theme(spacing.unit-8)] data-[pressed=true]:scale-[0.97] transition-transform-colors motion-reduce:transition-none bg-transparent border-default text-foreground w-full md:w-auto"
                  >
                    <FaPlay />
                    Watching video
                  </Link>
                </div>
              </div>
              <div className="md:mt-10">
                <div className="font-bold">Follow Us</div>
                <div className="mt-4">
                  {
                    followData.map((item, index) => {
                      return <Link href={item.link} key={item.link + "_" + index} className="text-inherit p-0">
                        <div className={`mr-4 w-[40px] h-[40px] rounded-full border hover:bg-success hover:text-white grid place-items-center ${!index ? 'bg-success text-white' : ''}`}>
                          {item.icon}
                        </div>
                      </Link>
                    })
                  }
                </div>
              </div>
            </div>
            <div className="relative">
              <div className="motion-reduce:transition-none inline-block animate-levitate absolute">
                <Image src="/home/banner/header-image.png" alt="" className="max-w-none" />
              </div>
            </div>
          </div>
          <div className="banner__shape">
            <div className="z-10 motion-reduce:transition-none absolute animate-rotate 2xl:left-auto xl:left-[52%] 2xl:right-[46%] xl:bottom-[20%] xl:top-auto lg:top-[71%] lg:right-[52%] md:top[80%] md:right[45%] xs:top[46%] xs:right[45%]">
              <Image src="/home/banner/4.png" alt="shape icon"/>
            </div>
          </div>
        </div>
      </div>
      
    </>
  )
}