'use client'

import { Image, Link } from "@nextui-org/react";
import { HTMLMotionProps } from "framer-motion";
import { FaFacebookF, FaLinkedinIn, FaInstagram, FaYoutube, FaTwitter } from 'react-icons/fa';
import { MenuItem, menuData } from "../navbar/data";

export type MotionProps = HTMLMotionProps<"div">;

export default function Footer () {

  const followData = [
    { icon: <FaFacebookF className="align-middle"/>, link: "#" },
    { icon: <FaLinkedinIn className="align-middle"/>, link: "#" },
    { icon: <FaInstagram className="align-middle"/>, link: "#" },
    { icon: <FaYoutube className="align-middle"/>, link: "#" },
    { icon: <FaTwitter className="align-middle"/>, link: "#" }
  ]

  return (
    <div className="footer-wrapper text-white">
      <div className="container mx-auto max-w-7xl md:pt-[4rem]">
        <div className="grid grid-cols-3 gap-4">
          <div className="col-span-1">
            <Image src="/logo/logo-dark.png" alt=""/>
            <p className="text-[.9rem] py-5 text-[#6B6F7F]" style={{
              maxInlineSize: "40ch"
            }}>
              Welcome to our trading site! We offer the best, most affordable products and services around. Shop now and start finding great deals!
            </p>
            <div className="flex">
              <div className="bg-[#00D094] flex items-center px-3 py-1 rounded-md text-[#0C263A] cursor-pointer mr-5">
                <Image src="/apple.png" alt="" className="mr-2"/>
                <div className="leading-[1rem]">
                  <p className="text-[.5rem]">Download on the</p>
                  <p className="mb-1 text-[0.875rem] font-bold">App Store</p>
                </div>
              </div>
              <div className="bg-[#124D6D] flex items-center px-3 py-1 rounded-md text-white cursor-pointer">
                <Image src="/play.png" alt="" className="mr-2"/>
                <div className="leading-[1rem]">
                  <p className="text-[.5rem]">GET IT ON</p>
                  <p className="mb-1 text-[0.875rem] font-bold">Google play</p>
                </div>
              </div>
            </div>
          </div>
          <div className="col-span-2">
            <div className="grid grid-cols-3 gap-3">
              {
                menuData.map((item: MenuItem, index: number) => {
                  if(index === 1 || index === 2 || index === 4 ) {
                    return <div key={item.name + "_" + index}>
                      <Link href={item.link} className="font-bold text-lg text-white">
                        {item.name}
                      </Link>
                      <div className="py-5">
                        {item.child?.map((child: MenuItem, i: number) => {
                          return <Link
                            className="text-[#6B6F7F] pb-4 hover:text-success-400 block"
                            href={child.link}
                            key={child.name + "_" + i}
                          >
                            {child.name}
                          </Link>
                        })}
                      </div>
                    </div>
                  }
                })
              }
            </div>
          </div>
        </div>
        <div className="border-t-[.5px] border-[#ffffff1a]">
          <div className="flex justify-between items-center">
            <div>
              <p className="text-xs inline-block mr-1 text-[#6B6F7F]">© 2023 All Rights Reserved By</p> 
              <Link href="#" className="color-success font-bold text-xs">Giangph</Link>
            </div>
            <div className="py-[2rem]">
              {
                followData.map((item, index) => {
                  return <Link href={item.link} key={item.link + "_" + index} className="text-inherit p-0">
                    <div className="mr-4 w-[40px] h-[40px] rounded-full border-2 hover:text-success hover:border-success grid place-items-center">
                      {item.icon}
                    </div>
                  </Link>
                })
              }
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}