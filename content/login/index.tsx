import React, { ChangeEvent, useState } from "react";
import {Modal, ModalContent, ModalHeader, ModalBody, ModalFooter, Button, Checkbox, Input, Link} from "@nextui-org/react";
import { FaLock, FaEnvelope, FaEye, FaEyeSlash,  } from "react-icons/fa";
import { PiGoogleLogoLight, PiXBold } from "react-icons/pi";
import { MdFacebook } from "react-icons/md";
import { useForm, Form } from "react-hook-form";
import useSWR from 'swr';

interface LoginModalPropsType {
  isOpen: boolean;
  onOpenChange: any;
}

type Inputs = {
  email: string
  password: string
  remember: boolean
}

export default function LoginModal({isOpen, onOpenChange}: LoginModalPropsType) {
  const fetcher = (url:string ,...args: any) => fetch(url, ...args).then(res => res.json());
  const {
    register,
    handleSubmit,
    watch,
    setValue,
    formState: { errors },
  } = useForm<Inputs>();
  
  const [isVisible, setIsVisible] = React.useState(false);
  const toggleVisibility = () => setIsVisible(!isVisible);
  const removeEmailField = () => {
    setValue('email', '');
  }

  const handleRemember = (e: ChangeEvent<HTMLInputElement>) => {
    const { checked } = e.target;
    setValue('remember', checked);
  }

  const handleSubmitForm = async (e:Inputs) => {
    const data = await fetcher('http://localhost:5000/api/v1/login', {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        username: e.email,
        password: e.password,
        ternant: 'admin'
      })
    });
    console.log(data);
  }

  return (
    <>
      <Modal 
        isOpen={isOpen} 
        onOpenChange={onOpenChange}
        placement="top-center"
      >
        <ModalContent>
          {(onClose) => (
            <>
              <ModalBody className="py-[3rem]">
                <h1 className="text-center font-bold text-[1.3rem] t">
                  Đăng nhập
                </h1>
                <div className="social-icons text-center py-3">
                  <a href="#" className="icon">
                    <PiGoogleLogoLight />
                  </a>
                  <a href="#" className="icon">
                    <MdFacebook />
                  </a>
                </div>
                <Form>
                  <Input
                    color="primary"
                    type="email"
                    startContent={
                      <FaEnvelope className="text-xl text-default-400 pointer-events-none flex-shrink-0" />
                    }
                    {...register("email")}
                    placeholder="Enter your email"
                    endContent={
                      <div className="cursor-pointer" onClick={removeEmailField}>
                        <PiXBold className="text-lg text-default-400 pointer-events-none" />
                      </div>
                    }
                  />
                  <Input
                    color="primary"
                    className="mt-3"
                    startContent={
                      <FaLock className="text-xl text-default-400 pointer-events-none flex-shrink-0" />
                    }
                    endContent={
                      <div className="cursor-pointer" onClick={toggleVisibility}>
                        {isVisible ? (
                          <FaEyeSlash className="text-lg text-default-400 pointer-events-none" />
                        ) : (
                          <FaEye className="text-lg text-default-400 pointer-events-none" />
                        )}
                      </div>
                    }
                    {...register("password")}
                    placeholder="Enter your password"
                    type={isVisible ? "text" : "password"}
                  />
                  <div className="flex py-3 px-1 justify-between">
                    <Checkbox
                      classNames={{
                        label: "text-small",
                      }}
                      onChange={handleRemember}
                    >
                      Remember me
                    </Checkbox>
                    <Link color="primary" href="#" size="sm">
                      Forgot password?
                    </Link>
                  </div>
                </Form>
              </ModalBody>
              <ModalFooter>
                <Button color="danger" variant="flat"  onPress={onClose}>
                  Close
                </Button>
                <Button color="primary" onClick={handleSubmit((e) => {
                  handleSubmitForm(e);
                })}>
                  Sign in
                </Button>
              </ModalFooter>
            </>
          )}
        </ModalContent>
      </Modal>
    </>
  );
}
