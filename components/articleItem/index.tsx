'use client'

import { Image, Link, Button, ButtonGroup } from "@nextui-org/react";
import {  ReactNode } from "react";
import { MdOutlineModeEditOutline, MdDeleteOutline, MdLink } from "react-icons/md";

interface ArticleItemsType {
  title: string;
  index: any;
  link: string;
  image: string;
  tag?: string;
  desc: string;
  author?: any;
  isSelect?: boolean;
  ids?: string[];
  isEdit?: boolean;
}

const ArticleItems = (props: ArticleItemsType): ReactNode => {
  const { title, index, link, image, tag, desc, author, isEdit, ids, isSelect } = props;
    return (
      <div className="px-3">
        <div className="relative group/item">
          <div className="article-action absolute hidden group-hover/item:flex justify-center w-full h-full z-10 bg-zinc-950/[.5] rounded-md cursor-pointer">
            <ButtonGroup className="mt-3 text-lg" size="sm">
              <Button color="warning">
                <MdOutlineModeEditOutline className="text-lg"/>
              </Button>
              <Button color="danger">
                <MdDeleteOutline className="text-lg"/>
              </Button>
              <Button color="primary">
                <MdLink className="text-lg"/>
              </Button>
            </ButtonGroup>
          </div>
          <Link 
            href={link}
            data-value={index + 1}
            className="text-current shadow-md mb-2 transition-colors ease-in-out bg-white rounded-lg border-2 border-transparent hover:border-success py-[2em] px-7 block"
          >
            <div className="overflow-hidden rounded-md">
              <div 
                className={`w-100 pb-[60%] bg-cover bg-no-repeat bg-center slide-bg-scale transition-transform ease-in-out`} 
                style={{ backgroundImage: "url('" + image + "')" }}
              ></div>
            </div>
            <div className="pricings-content-price py-1 text-xs font-bold px-3 color-primary bg-[#F1F4FB] mb-4 mt-6 inline-block rounded-md">
              <h3>{tag}</h3>
            </div>
            <h2 className="pricings-content-title font-bold text-[1.2rem] pb-4">{title}</h2>
            <p className="pricings-content-services pb-4 text-sm text-inherit"> {desc.length > 90 ? desc.substring(0, 90) + "..." : desc }</p>
            <div className="flex items-center">
              <Image src={author.avt} alt="" className="mr-2 z-1"/>
              <div className="">
                <div className="font-bold">{ author.name }</div>
                <span className="text-xs text-inherit">12/07/2023</span>
              </div>
            </div>
          </Link>
        </div>
      </div>
    )
    
}

export default ArticleItems;