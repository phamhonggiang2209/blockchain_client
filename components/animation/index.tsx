"use client";

import { HTMLMotionProps, motion, useAnimation, useInView } from "framer-motion";
import React, { ReactHTML, ReactNode, useEffect, useRef } from "react";

interface AnimationProps {
  rootMargin: string;
  children: ReactNode;
  variants: {
    hidden: any;
    visible: any;
  };
  key?: any;
}


export function Animation({ rootMargin, children, variants, ...props }: AnimationProps) {
  const ref = useRef(null);
  const inViewControls = useAnimation();
  const inView = useInView(ref, {
    margin: "0px 0px " + rootMargin + " 0px",
    once: true
  });

  useEffect(() => {
    inViewControls.start(inView ? 'visible' : 'hidden');
  }, [inView, inViewControls]);

  return (
    <motion.div {...props} ref={ref} initial="hidden" animate={inViewControls} variants={variants}>
      {children}
    </motion.div>
  );
}