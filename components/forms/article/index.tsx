/* eslint-disable @next/next/no-img-element */
"use client";

import { ReactNode, useRef } from "react";
import { Input, Textarea, Select, SelectItem, Button } from "@nextui-org/react";
import { HTMLMotionProps } from "framer-motion";
import { Formik, Form } from "formik";
import HorizontalItem from "../input";
import { ARTICLE_TYPE } from "@/utils/constant";
import MyEditor from "../editor";

export type MotionProps = HTMLMotionProps<"div">;

export default function ArticleForm(props: any): ReactNode {
  const { mode } = props;
  const editorRef = useRef(null);
  const submitForm = (values: any) => {
    console.log(values);
  };

  const handleChangeEditor = () => {
    
  }

  return (
    <>
      <div className="relative bg-white pt-[6rem]">
        <div className="category-banner relative">
          <img
            src="/category-banner.png"
            alt=""
            className="w-[100%] rounded-none"
          />
          <div className="absolute w-full h-full top-0 left-0 flex items-center">
            <div className="container mx-auto max-w-7xl text-white mx-auto">
              <div className="ml-3">
                <h1 className="inline font-semibold text-[3rem] lg:leading-[4rem]">
                  Tạo mới bài viết
                </h1>
              </div>
            </div>
          </div>
        </div>
        <div className="article-detail container mx-auto max-w-7xl mx-auto py-[3rem]">
          <Formik initialValues={{}} onSubmit={submitForm}>
            <Form>
              <HorizontalItem label="Chọn loại bài viết: "> 
                <Select 
                  color="success"
                  variant="bordered"
                  name="title"
                  labelPlacement="outside"
                  placeholder="Nhập tên bài viết"
                >
                  {ARTICLE_TYPE.map((type: any, index: number) => {
                    return <SelectItem key={type.name} value={type.value}>
                      {type.name}
                    </SelectItem>
                  })}
                </Select>
              </HorizontalItem>
              <HorizontalItem label="Nhập tên bài viết: "> 
                <Input 
                  color="success"
                  variant="bordered"
                  name="title" 
                  type="text" 
                  labelPlacement="outside"
                  placeholder="Nhập tên bài viết"
                />
              </HorizontalItem>
              <HorizontalItem label="Nhập mô tả: "> 
                <Textarea
                  className="mt-0"
                  color="success"
                  variant="bordered"
                  labelPlacement="outside"
                  placeholder="Nhập mô tả"
                />
              </HorizontalItem>
              <HorizontalItem label="Nhập nội dung: "> 
                <MyEditor ref={editorRef} onEditorChange={handleChangeEditor} />
              </HorizontalItem>
              <HorizontalItem label="Chọn tên tag: "> 
                <Select 
                  color="success"
                  variant="bordered"
                  name="tag"
                  labelPlacement="outside"
                  placeholder="Chọn tag"
                  multiple
                >
                  {ARTICLE_TYPE.map((type: any, index: number) => {
                    return <SelectItem key={type.name} value={type.value}>
                      {type.name}
                    </SelectItem>
                  })}
                </Select>
              </HorizontalItem>
              <div className="flex gap-3 justify-end mb-5 pt-[3rem]">
                <Button type="submit" color="success">
                  Submit
                </Button>
                <Button color="default">Hủy</Button>
              </div>
            </Form>
          </Formik>
        </div>
      </div>
    </>
  );
}
