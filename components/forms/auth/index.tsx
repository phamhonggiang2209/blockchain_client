'use client';

import { Button, Input } from "@nextui-org/react";
import { Form, Formik } from "formik";
import { useState } from "react";
import { MdFacebook } from "react-icons/md";
import { PiGoogleLogoLight } from "react-icons/pi";

const AuthForm = (props: any): any => {
  const submitForm = (value: any) => {};
  const [changeForm, setChangeForm] = useState<boolean>(false);
  return (
    <>
      <div className="container auth-form-container" id="auth-form-container">
        <div className="form-container sign-up">
          <Formik
            initialValues={{
              email: "",
              fullname: "",
              phone_number: "",
              password: "",
              password_confirm: "",
            }}
            onSubmit={submitForm}
          >
            {({ values, handleChange, handleSubmit, isSubmitting }) => (
              <Form onSubmit={handleSubmit}>
                <h1>Create Account</h1>
                <div className="social-icons">
                  <a href="#" className="icon">
                    <PiGoogleLogoLight />
                  </a>
                  <a href="#" className="icon">
                    <MdFacebook />
                  </a>
                </div>
                <span>or use your email for registeration</span>
                <Input type="text" className="mb-2" placeholder="Name" />
                <Input type="email" className="mb-2" placeholder="Email" />
                <Input type="password" className="mb-2" placeholder="Password" />
                <Button>Sign In</Button>
              </Form>
            )}
          </Formik>
        </div>
        <div className="form-container sign-in">
          <Formik
            initialValues={{
              email: "",
              fullname: "",
              phone_number: "",
              password: "",
              password_confirm: "",
            }}
            onSubmit={submitForm}
          >
            {({ values, handleChange, handleSubmit, isSubmitting }) => (
              <Form onSubmit={handleSubmit}>
                <h1>Sign In</h1>
                <div className="social-icons">
                  <a href="#" className="icon">
                    <PiGoogleLogoLight />
                  </a>
                  <a href="#" className="icon">
                    <MdFacebook />
                  </a>
                </div>
                <span>or use your email password</span>
                <Input type="email" className="my-2" placeholder="Email" />
                <Input type="password" className="mb-2" placeholder="Password" />
                <a href="#">Forget Your Password?</a>
                <Button>Sign In</Button>
              </Form>
            )}
          </Formik>
        </div>
        <div className="toggle-container">
          <div className="toggle">
            <div className="toggle-panel toggle-left">
              <h1>Welcome Back!</h1>
              <p>Enter your personal details to use all of site features</p>
              <Button className="hidden" id="login">
                Sign In
              </Button>
            </div>
            <div className="toggle-panel toggle-right">
              <h1>Hello, Friend!</h1>
              <p>
                Register with your personal details to use all of site features
              </p>
              <Button className="" id="register">
                Sign Up
              </Button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default AuthForm;
