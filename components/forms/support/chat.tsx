'use client'

import { Textarea } from "@nextui-org/react";
import React, { useState, useRef, useEffect } from "react";
import { MdWavingHand, MdOutlineImage, MdAttachFile, MdSend} from "react-icons/md";
import { CiFaceSmile } from "react-icons/ci";
import EmojiPicker from 'emoji-picker-react';

export default function SupportMessageForm(props: any) {
  const { isVisibleForm } = props;
  const [bodyHeight, setBodyHeight] = useState<number>(300);
  const [toggleEmoji, setToggleEmoji] = useState<boolean>(false);
  const supportMessageHeaderRef = useRef(null);
  const supportMessageBodyRef = useRef(null);
  const supportMessageHeight = useRef(null);
  const supportMessageInputRef = useRef(null);

  const handleToggleEmoji = () => setToggleEmoji(!toggleEmoji);

  const pickEmoji = (e: any) => {
    console.log(e);
  }

  const avtList = [
    "/avatar/1.png",
    "/avatar/2.png",
    "/avatar/1.png",
    "/avatar/1.png",
    "/avatar/1.png",
    "/avatar/1.png",
    "/avatar/1.png",
    "/avatar/1.png",
    "/avatar/1.png",
    "/avatar/1.png"
  ];
  
  useEffect(() => {
    if(supportMessageHeaderRef.current && supportMessageInputRef.current && supportMessageHeight.current) {
      const headerHeight: number = supportMessageHeaderRef.current['clientHeight'] || 0;
      const inputHeight: number = supportMessageInputRef.current['clientHeight'] || 0;
      const formHeight: number = supportMessageHeight.current['clientHeight'] || 0;
      setBodyHeight(formHeight - (headerHeight + inputHeight + 100));
    }
  }, []);

  return (
    <>
      <div
        className={`message-wrapper fixed bottom-0 right-0 w-1/4  ${isVisibleForm ? 'opacity-100 z-40' : 'opacity-0'}`} 
        ref={supportMessageHeight}
      >
        <div className="message-content h-100 border">
          <div className="message-content-header bg-success p-[2rem]" ref={supportMessageHeaderRef}>
            <div className="message-content-heade-title text-white">
              <div className="font-bold items-center flex">Học trading Việt Nam xin chào 
               <MdWavingHand className="ml-2 font-bold text-warning" />
              </div>
              <p className="text-[.9rem] mt-3">Chúng tôi rất sẵn lòng được hỗ trợ bạn. Vui lòng đặt câu hỏi ở dưới.</p>
            </div>

            <div className="flex items-center mt-3">
              <div className="list-argent flex mr-3">
                {
                  avtList.slice(0, 2).map((items: string, index: number) => {
                    return <div key={index} className="argent w-[45px] h-[45px] bg-cover bg-center bg-no-repeat border-2 rounded-full shadow-md border-success" style={{
                      marginLeft: index ? "-12px" : "",
                      backgroundImage: `url('${items}')`
                    }}>
                    </div>
                  })
                }
                {
                  (avtList.length > 3 && <div key="1231232343" className="argent ml-[-12px] w-[45px] h-[45px] grid place-items-center bg-black/[.6] text-white border-2 rounded-full shadow-md border-success ">
                    +{avtList.length - 3}
                  </div>)
                }
              </div>
              <div className="start-chat text-[.9rem] text-white">
                <div className="font-bold">Hỗ trợ viên đang trực tuyến</div>
                <span>sẽ phản hồi bạn trong vài phút</span>
              </div>
            </div>
          </div>

          <div className="message-content-body bg-white px-3" ref={supportMessageBodyRef} style={{
            minHeight: bodyHeight + "px"
          }}>

          </div>
          <div className="message-content-input cursor-pointer bg-white p-3" ref={supportMessageInputRef}>
            <div className="flex items-center">
              <Textarea placeholder="Nhập tin trả lời" maxRows={1} variant="bordered" color="success" className="mr-3 inputWrapper-border-1"/>
              <div className="flex items-center gap-3 text-lg">
                <MdOutlineImage className="hover:text-success"/>
                <MdAttachFile className="hover:text-success"/>
                <div className="relative">
                  <div className={`absolute bottom-full block right-0 mb-5 ${ toggleEmoji ? 'opacity-100' : 'opacity-0'}`}>
                    <EmojiPicker onEmojiClick={pickEmoji}/>
                  </div>
                  <CiFaceSmile className={`hover:text-success ${toggleEmoji ? 'text-success' : ''}`} onClick={handleToggleEmoji}/>
                </div>
                <MdSend className="hover:text-success"/>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
