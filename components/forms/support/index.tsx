'use client'

import { Button, Link } from "@nextui-org/react";
import React, { useState, useRef, useEffect } from "react";
import { BsFillChatDotsFill } from "react-icons/bs";
import { MdWavingHand, MdSend, MdOutlineArrowRightAlt, MdCall } from "react-icons/md"
import { FaFacebookMessenger, FaFacebookF } from "react-icons/fa";
import { SiZalo } from "react-icons/si";
import { motion, LazyMotion, domAnimation } from "framer-motion";
import SupportMessageForm from "./chat";

export default function SupportForm() {
  const [isVisible, setIsVisible] = useState<boolean>(false);
  const [isVisibleForm, setIsVisibleForm] = useState<boolean>(false);
  const [formActive, setFormActive] = useState<boolean>(false);
  const [bodyHeight, setBodyHeight] = useState<number>(300);
  const supportMessageHeaderRef = useRef(null);
  const supportMessageBodyRef = useRef(null);
  const supportMessageButtonRef = useRef(null);
  const supportMessageHeight = useRef(null);

  const toggleVisibility = () => {
    if (formActive || isVisibleForm) {
      setIsVisibleForm(!isVisibleForm);
    } else {
      setIsVisible(!isVisible);
    }
  }

  const handleShowMessageForm = () => {
    setIsVisible(false);
    setIsVisibleForm(true);
    setFormActive(true);
  }

  const handleHideMessageForm = () => {
    setIsVisible(true);
    setIsVisibleForm(false);
    setFormActive(false);
  }

  const handleCloseForm = () => {
    setIsVisibleForm(false);
    setFormActive(false);
  }

  const avtList = [
    "/avatar/1.png",
    "/avatar/2.png",
    "/avatar/1.png",
    "/avatar/1.png",
    "/avatar/1.png",
    "/avatar/1.png",
    "/avatar/1.png",
    "/avatar/1.png",
    "/avatar/1.png",
    "/avatar/1.png"
  ];

  const contactList = [
    {
      name: "Liên hệ qua Messenger",
      icon: <FaFacebookMessenger />,
      link: ""
    },
    {
      name: "Hotline: 0977006620 (8h00 - 22h00)",
      icon: <MdCall />,
      link: ""
    },
    {
      name: "Liên hệ qua Zalo QA",
      icon: <SiZalo />,
      link: ""
    },
    {
      name: "Fanpage HOCTRADING VIETNAM",
      icon: <FaFacebookF />,
      link: ""
    }
  ];

  useEffect(() => {
    if(supportMessageHeaderRef.current && supportMessageButtonRef.current && supportMessageHeight.current) {
      const headerHeight: number = supportMessageHeaderRef.current['clientHeight'] || 0;
      const buttonHeight: number = supportMessageButtonRef.current['clientHeight'] || 0;
      const formHeight: number = supportMessageHeight.current['clientHeight'] || 0;
      setBodyHeight(formHeight - (headerHeight + buttonHeight + 30));
    }

  }, []);

  return (
    <LazyMotion features={domAnimation}>
      <div
        className={`message-wrapper fixed bottom-0 right-0 w-1/4  ${isVisible ? 'opacity-100 z-50' : 'opacity-0'}`} 
        ref={supportMessageHeight}
      >
        <div className="message-content h-100 border">
          <div className="message-content-header bg-success" ref={supportMessageHeaderRef}>
            <div className="message-content-heade-title text-white px-[3rem] py-[4rem]">
              <div className="font-bold items-center flex">Xin chào 
              <MdWavingHand className="ml-2 font-bold text-warning" />
              </div>
              <p className="text-[.9rem] mt-3">Chúng tôi rất sẵn lòng được hỗ trợ bạn. Vui lòng đặt câu hỏi ở dưới.</p>
            </div>
          </div>
          <div className="message-content-body overflow-auto bg-white" style={{minHeight: `${bodyHeight}px`}} ref={supportMessageBodyRef}>
            <div className="p-3">
              <div className="p-4 border bg-white">
                <div className="text-black font-bold">Chat với chúng tôi</div>
                <span className="text-[.9rem] py-3 block">Hỗ trợ viên đang trực tuyến sẵn sàng hỗ trợ</span>
                <div className="flex justify-between items-center">
                  <div className="list-argent flex">
                    {
                      avtList.slice(0, 2).map((items: string, index: number) => {
                        return <div key={index} className="argent w-[45px] h-[45px] bg-cover bg-center bg-no-repeat border-2 rounded-full shadow-md border-success" style={{
                          marginLeft: index ? "-12px" : "",
                          backgroundImage: `url('${items}')`
                        }}>
                        </div>
                      })
                    }
                    {
                      (avtList.length > 3 && <div key="1231232343" className="argent ml-[-12px] w-[45px] h-[45px] grid place-items-center bg-black/[.6] text-white border-2 rounded-full shadow-md border-success ">
                        +{avtList.length - 3}
                      </div>)
                    }
                  </div>
                  <div className="start-chat">
                    <Button color="success" startContent={<MdSend/>} onClick={handleShowMessageForm}>
                      Bắt đầu chat
                    </Button>
                  </div>
                </div>
              </div>
            </div>

            <div className="p-3">
              <div className="p-4 border bg-white">
                <div className="text-black font-bold">Liên hệ qua kênh ưa thích của bạn</div>
                <div className="list-contact mt-4">
                  {
                    contactList.map((items: any, index: number) => {
                      return <Link
                        href={items.link}
                        key={index} 
                        className="contact-item border-1 hover:text-primary hover:border-primary rounded mb-3 flex items-center justify-between p-2 text-current"
                      >
                        <div className="flex items-center gap-2">
                          <div className="grid place-items-center text-white bg-primary p-1 rounded-full">{items.icon}</div>
                          <span className="text-[.9em]">{items.name}</span>
                        </div>
                        <MdOutlineArrowRightAlt className="text-md" />
                      </Link>
                    })
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <SupportMessageForm 
        isVisibleForm={isVisibleForm}
        hideMessageForm={handleHideMessageForm}
        clostForm={handleCloseForm}
      />
      <div className="message-button fixed flex justify-end pr-[3rem] py-4 z-50 bottom-0 right-0" ref={supportMessageButtonRef}>
        <motion.div
          whileTap={{ scale: 0.97 }}
          whileHover={{ scale: 1.2 }}
          className="w-[50px] h-[50px] inline-block rounded-full bg-success grid place-items-center text-white font-bold cursor-pointer" 
          onClick={toggleVisibility}
        >
          <BsFillChatDotsFill />
        </motion.div>
      </div>
    </LazyMotion>
  );
}
