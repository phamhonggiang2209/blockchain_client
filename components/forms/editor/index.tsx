
import React from "react";
import { Editor } from "@tinymce/tinymce-react";
import { EditorConfig } from "@/utils/config";
import { forwardRef } from "@nextui-org/react";

export const MyEditor = forwardRef(
  ({onEditorChange, initValue}: any, ref: any) => {
    return (
      <Editor
        apiKey='l8awrmmzpc403s4hcsn95w9o3ca3t4vky24dpzoceijagr6g'
        onInit={(evt, editor) => ref.current = editor}
        onEditorChange={onEditorChange}
        initialValue={initValue}
        init={{...EditorConfig, ai_request: (request: any, respondWith: any) => respondWith.string(() => Promise.reject("See docs to implement AI Assistant")),}} 
      />
    );
  },
);

export default MyEditor;