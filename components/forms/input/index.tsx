'use client'

import { Textarea } from "@nextui-org/react";
import React, { useState, useRef, useEffect } from "react";
import { MdWavingHand, MdOutlineImage, MdAttachFile, MdSend} from "react-icons/md";
import { CiFaceSmile } from "react-icons/ci";
import EmojiPicker from 'emoji-picker-react';
import { Input } from "postcss";

interface MyInputProps {
  label: string;
  children: React.ReactNode
}

export default function HorizontalItem(props: MyInputProps) {
  const {label, children} = props;
  return (
    <>
      <div className="grid grid-cols-4 gap-4 items-center mb-4">
        <label className="font-bold">{label}</label>
        <div className="col-span-3">
          {children}
        </div>
      </div>
    </>
  );
}
