export const ARTICLE_DETAIL_MODE = {
  CREATE: 1,
  VIEW: 2,
  UPDATE: 3,
};

export const ARTICLE_TYPE_VIDEO = 1;
export const ARTICLE_TYPE_TEXT = 1;

export const ARTICLE_TYPE = [
  {value: ARTICLE_TYPE_VIDEO, name: "Video"},
  {value: ARTICLE_TYPE_TEXT, name: "Văn bản"}
]