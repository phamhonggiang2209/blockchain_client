export const urlToName = (str: string): string => {
  if(!str) return "";
  return str.replace(/\-/g, " ").replace(/^/, s => s.toUpperCase());
}